<?php
	
	// Variables
	$name = 'John Smith';
	$email = 'johnsmith@gmail.com';

	// Constants (Read Only Variables)
	define('PI', 3.1416);

	// Data Types

	// Strings
	$state = 'New York';
	$country = 'United States of America';
	// Contaenation of strings
	// $address = $state.','.$country;
	$address = "$state, $country";

	// Integers
	$age = 31;
	$headcount = 26;

	// Decimal
	$grade = 98.2;
	$distanceInKilometers = 1324.34;

	// Boolean
	$hasTravelledAbroad = false;
	$haveSymptoms = true;

	// null
	$boyfriend = null;
	$girlfriend = null;

	// Arrays (data type: object)
	$grades = array(90.1, 87, 98, 93);

	// Objects
	$gradesObj = (object) [
							'firstGrading' => 98.7,
							'secondGrading' => 92.1,
							'thirdGrading' => 90.2,
							'fourthGrading' => 95
	];

	$personObj = (object) [
							'fullName' => 'John Doe',
							'isMarried' => false,
							'age' => 35,
							'address' => (object) [
								'state' => 'New York',
								'country' => 'United States of America'
							]
	];

	// Operators

	$x = 1342.14;
	$y = 1268.24;

	$isLegalAge = true;
	$isRegistered = true;

	// Functions
	// parameters are containers/storage while arguments are the actual value stored in the parameter

	// Function Declaration
	function getFullName($firstName, $middleInitial, $lastName) {
		return "$lastName, $firstName, $middleInitial";
	}

	// Selection Control Structures

	// If-Elseif-Else Statement

	function determineTyphoonIntensity($windspeed) {
		if ($windspeed < 30) {
			return 'Not a typhoon yet';
		}
		else if ($windspeed <=61) {
			return 'Tropical Depression Detected';
		}
		else if ($windspeed >=62 && $windspeed <=88) {
			return 'Tropical Storm Detected';
		}
		else if ($windspeed >=89 && $windspeed <= 117) {
			return 'Severe Tropical Storm Detected';
		}
		else {
			return 'Typhoon Detected';
		}
	}

	// Conditional (Ternary) Operator

	function isUnderAge($age) {
		return ($age < 18) ? true : false;
	}

	// Switch Statement

	function determineComputerUser($computerNumber) {
		switch($computerNumber) {
			case 1: 
				return 'Linus Torvalds'; break;
			case 2:
				return 'Steve Jobs'; break;
			case 3: 
				return 'Sid Meier'; break;
			case 4:
				return 'Onel De Guzman'; break;
			case 5:
				return 'Christian Salvador'; break;
			default:
				return $computerNumber.' is out of bounds.';break;
		}
	}

	// Try-Catch-Finally Statement

	function greeting($str) {
		try {
			// attempt to execute a code
			if (gettype($str) == "string") {
				echo $str;
			}
			else {
				throw new Exception("Oooopss!");
			}
		}
		catch (Exception $e) {
			// Catch the errors within "try"
			echo $e->getMessage();
		}
		finally {
			// regardless if success or fail, it will execute the block code
			echo "I did it again!";
		}
	}

?> 
<!-- Closing tags in php are optional -->