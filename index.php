<!-- Ideal place to connect php files -->
<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="widtd=device-width initial-scale=1">
		<title>s01: PHP Basics and Selection Control Structures</title>
	</head>
	<body>
		<!-- <h1>Hello World!</h1> -->

		<h1>Echoing Values</h1>

		<p>
			<?php echo 'Good day $name! Your given email is $email' ?>
		</p>

		<p>
			<?php
				// Variables only work in double quotations
				echo "Good day $name! Your given email is $email"
			?>
		</p>

		<p>
			<?php echo PI ?>
		</p>

		<p>
			<?php echo $address ?>
			<?php echo $age ?>
		</p>

		<p>
			<!-- To output the value of an object property, the arrow notation can be used -->
			<?php echo $gradesObj->firstGrading; ?>
			<?php echo $personObj->address->state ?>
		</p>

		<p>
			<?php echo gettype($hasTravelledAbroad); ?>
		</p>

		<p>
			<?php echo var_dump($girlfriend); ?>
		</p>

		<p>
			<?php echo $grades[2]; ?>
		</p>

		<h1>Operators</h1>

		<h2>Arithmetic Oprators</h2>

		<p>Sum: <?php echo $x + $y; ?></p>
			
		<h2>Equality Operators</h2>

		<p>Loose Equality: <?php echo var_dump($x == 1342.14); ?></p>
		<p>Loose Equality: <?php echo var_dump($x != 1342.14); ?></p>

		<h2>Greater/Lesser Operators</h2>

		<p>is greater: <?php echo var_dump($x > $y); ?></p>

		<h2>Logical Oprators</h2>

		<p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>

		<h1>Function</h1>

		<p>Full Name: <?php echo getFullName('John', 'D.', 'Smith') ?></p>

		<h2>If-Elseif-Else</h2>
		<p><?php echo determineTyphoonIntensity(35) ?></p>

		<h2>Ternary</h2>
		<p>78: <?php echo var_dump(isUnderAge(78)); ?></p>

		<h2>Switch</h2>

		<p><?php echo determineComputerUser(4); ?></p>

		<h2>Try-Catch-Finally</h2>
		<p><?php echo greeting(3); ?></p>
	</body>
</html>